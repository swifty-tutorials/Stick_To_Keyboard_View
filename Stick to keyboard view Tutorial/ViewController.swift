//
//  ViewController.swift
//  Stick to keyboard view Tutorial
//
//  Created by Sobhan Eskandari on 8/10/18.
//  Copyright © 2018 Aurora. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Setting placeholder color of fields
        if let userplaceholder = usernameField.placeholder,let passPlaceholder = passwordField.placeholder {
            usernameField.attributedPlaceholder = NSAttributedString(string:userplaceholder,
            attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1, alpha: 0.6)])
            passwordField.attributedPlaceholder = NSAttributedString(string:passPlaceholder,
            attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1, alpha: 0.6)])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

